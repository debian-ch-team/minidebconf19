# miniDebConf Vaumarcus IRC meeting #1

It happened on #debian.ch@irc.oftc.net, from 20:00h CEST to 21:40h CEST.

## Meeting minutes

* Intro - Who's there

OdyX, Jathan, XTaran, alphanet

* Opening registration
  * `#info` See https://wiki.debian.org/DebianEvents/ch/2019/Vaumarcus and https://wiki.debian.org/DebianEvents/ch/2019/Vaumarcus/HowToGetThere
  * `#action` OdyX to work on the Registration page, mention what the attendees need to bring, and email debian-devel-announce and debian-events-eu. Prices will have to wait.
  * `#action` OdyX to clarify the extent of the debian.ch support towards the event with the debian.ch board.

* Finances
  * `#info` We have two full buildings, a projector and screen for 2210 CHF. Add 75.- per day per person for a Night, a Stay tax and a Meal day-pack.

* Sponsors
  * Sponsor ideas
    * MACD GmbH https://macd.com/ Possible sponsor since their usage of Debian and Free Software based solutions in Aachen and Urdorf. We need to contact to Raphael Grochtmann and George Macdonald sending them the sponsorship invitation
    * Infomaniak
    * Liip SA
    * Puzzle ITC
    * UBS (XTaran has one Debian-related contact)
    * Nine
    * SWITCH
    * info@cril.ch
    * All the DebConf13 sponsors basically. :-)
  * `#action` OdyX to try convincing hug to take on Sponsoring, or find someone else. Last resort, do it.

* Schedule
  * `#action` XTaran will come up with probably wiki-based workflow for collecting and sorting talk submissions. Role model could be https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg#talk_schedule_v1.0.1
  * `#action` OdyX to clarify buildings' access before Friday (Thursday 14:30)
  * `#info` for now, the contract covers Friday 14:30 until Sunday 18:00.
  * `#info` let's coordinate a late Sunday dinner for those interested on the spot.

* Video Team
  * `#info` alphanet interested in seeing how it works, but so far not enough knowledge, may have some video hardware available or people for camera
  * `#action` jathan to check with the Video Team to see if something will happen from the DebConf Video Team.
  * `#info` there's possibility to get help from the CCC Zurich VOC.

* Organization
  * `#info` Next meeting Wednesday 4. September, 20h CEST.

* Bar
  * `#action` OdyX to check with Le Camp what they offer / what are their conditions.
  * `#action` OdyX to check for a tap-beer offer.
  * `#info` alphanet: OdyX: first, ask Le Camp what is doable, and when you know, I will investigate further

* Varia

## Actions, per person

* OdyX
  * `#action` OdyX to work on the Registration page, mention what the attendees need to bring, and email debian-devel-announce and debian-events-eu. Prices will have to wait.
  * `#action` OdyX to clarify the extent of the debian.ch support towards the event with the debian.ch board.
  * `#action` OdyX to try convincing hug to take on Sponsoring, or find someone else. Last resort, do it.
  * `#action` OdyX to clarify buildings' access before Friday (Thursday 14:30)
  * `#action` OdyX to check with Le Camp what they offer / what are their conditions.
  * `#action` OdyX to check for a tap-beer offer.
* XTaran
  * `#action` XTaran will come up with probably wiki-based workflow for collecting and sorting talk submissions. Role model could be https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg#talk_schedule_v1.0.1
 
* jathan
  * `#action` jathan to check with the Video Team to see if something will happen from the DebConf Video Team.

* alphanet

## Full IRC log

```
2019-08-20 18:01:05	ℹ 	OdyX has changed topic for #debian.ch from "miniDebConf Vaumarcus - https://wiki.debian.org/DebianEvents/ch/2019/Vaumarcus | Next IRC meeting: Aug. 20, 20h CEST — Helvetic Debian | All languages and dialects welcome | https://wiki.debian.org/LocalGroups#Switzerland | https://debian.ch/ | Umbrella, Shirts, Stickers: https://debian.ch/merchandise" to "miniDebConf Vaumarcus IRC meeting | #topic Intro Who's there?"
2019-08-20 18:01:21	@OdyX	#info Agenda and collaborative minutes are there:  https://mensuel.framapad.org/p/minidebconfvaumarcus-20190820
2019-08-20 18:01:29	@OdyX	So who's here?
2019-08-20 18:01:34	@OdyX	Didier Raboud
2019-08-20 18:01:36	jathan	I am
2019-08-20 18:01:42	jathan	Jonathan Bustillos
2019-08-20 18:02:10	axhn	<-- just curious and watching
2019-08-20 18:02:14	@OdyX	I expected XTaran and hug to show up.
2019-08-20 18:04:40	OdyX_	I just SMS'ed them both.
2019-08-20 18:05:13	jathan	Ok.
2019-08-20 18:05:21	@OdyX	Let's wait until 20:10
2019-08-20 18:05:32	 	XTaran (~abe@00027f51.user.oftc.net) has joined #debian.ch
2019-08-20 18:05:39	XTaran	OdyX: Thanks for the reminder!
2019-08-20 18:05:58	XTaran	Sorry, didn't hear my alarm at the Chaos Communication Camp.
2019-08-20 18:06:02	@OdyX	Ah, good :-)
2019-08-20 18:06:18	@OdyX	If you see a hug around :-)
2019-08-20 18:06:32	XTaran	Haven't seen him yet.
2019-08-20 18:06:52	@OdyX	(Just Signal'ed hug, so let's wait until 20:10
2019-08-20 18:06:54	@OdyX	)
2019-08-20 18:10:47	@OdyX	Ok; let's proceed.
2019-08-20 18:11:03	ℹ 	OdyX has changed topic for #debian.ch from "miniDebConf Vaumarcus IRC meeting | #topic Intro Who's there?" to "miniDebConf Vaumarcus IRC meeting | #topic Registration"
2019-08-20 18:11:27	@OdyX	So, as you have certainly seen; https://wiki.debian.org/DebianEvents/ch/2019/Vaumarcus has a logo and some information;
2019-08-20 18:11:35	⚡	XTaran waves from the Chaos Communication Camp!
2019-08-20 18:11:46	@OdyX	I took the DebConf13 page and made https://wiki.debian.org/DebianEvents/ch/2019/Vaumarcus/HowToGetThere
2019-08-20 18:11:47	XTaran	OdyX: Yes, saw it and looks cool!
2019-08-20 18:12:11	jathan	OdyX: It was a great idea.
2019-08-20 18:12:18	@OdyX	, but I didn't really want to open registration without a little more support than just my gut feeling.
2019-08-20 18:12:49	@OdyX	I had also done a preliminary budget on the -private rep
2019-08-20 18:12:51	@OdyX	o
2019-08-20 18:13:32	@OdyX	So the question is what is needed to make Registration happen.
2019-08-20 18:13:35	jathan	When we go to Sponsors, I will suggest one that could help with money.
2019-08-20 18:13:45	@OdyX	(Please contribute on the minutes directly
2019-08-20 18:13:50	@OdyX	https://mensuel.framapad.org/p/minidebconfvaumarcus-20190820)
2019-08-20 18:14:03	jathan	Ok.
2019-08-20 18:15:04	@OdyX	If we're 50, Le Camp (accom+meals) is going to cost ~8400 CHF all-in-all.
2019-08-20 18:16:14	@OdyX	It's not entirely linear, but its' between 165.- (with 50 persons) and 195.- (with 20 persons).
2019-08-20 18:16:48	@OdyX	So, do you think we need a rough price first, or should we open registration ASAP, and spell out a price later, if at all?
2019-08-20 18:17:13	XTaran	Hmmm, good question...
2019-08-20 18:18:07	@OdyX	XTaran: I have a vague recollection that we(debian.ch) wanted to cover a (large?) amount, or as fallback at least; what are your souvenirs from the AGM?
2019-08-20 18:18:38	XTaran	OdyX: Phew, that's months ago. Do we have minutes? :-)
2019-08-20 18:18:47	@OdyX	https://salsa.debian.org/debian-ch-team/docs/raw/master/meetings/2019/20190216_agm_minutes.txt 
2019-08-20 18:18:49	@OdyX	terse…
2019-08-20 18:19:57	@OdyX	What about we make sure the Registration page collects enough data, gives a range of financial options, but we announce "this week" that we want people to register, and will announce final prices later?
2019-08-20 18:20:16	@OdyX	(And I consult with the rest of the board to get a more precise view "soon")
2019-08-20 18:21:14	@OdyX	FTR, debian.ch clearly has the shoulders; but it's a political decision :-)
2019-08-20 18:21:16	XTaran	OdyX: Sounds good. I at least think that we should do an announcement of the event rather soon.
2019-08-20 18:21:45	@OdyX	It was announced in March https://lists.debian.org/debian-events-eu/2019/03/msg00004.html but yes, I agree.
2019-08-20 18:22:10	jathan	I think we should start opening the registration first and prices later.
2019-08-20 18:22:21	@OdyX	#action OdyX to work on the Registration page, and email debian-devel-announce and debian-events-eu. Prices will have to wait.
2019-08-20 18:22:32	XTaran	OdyX: Yeah, but people start to think it was a dead birth.
2019-08-20 18:22:58	@OdyX	I can't carry the project alone, but I don't want it dead either :-)
2019-08-20 18:22:58	XTaran	jathan: I agreee.
2019-08-20 18:23:09	XTaran	OdyX: Understandable.
2019-08-20 18:23:15	jathan	OdyX: We are here :)
2019-08-20 18:23:16	@OdyX	Okay, I got what I needed for Registration I guess.
2019-08-20 18:23:29	ℹ 	OdyX has changed topic for #debian.ch from "miniDebConf Vaumarcus IRC meeting | #topic Registration" to "miniDebConf Vaumarcus IRC meeting | #topic Finances"
2019-08-20 18:24:12	@OdyX	Let's just recap what we know. Le Camp expenses are going to be most of what we'll need (venue, accom, food).
2019-08-20 18:24:32	@OdyX	Of course it leaves out things like the bar, video team expenses and stuff.
2019-08-20 18:24:35	XTaran	So food is included in the prices you mentioned?
2019-08-20 18:26:26	@OdyX	We have two full buildings, a projector and screen for 2210 CHF. Add 75/- per day per person for a Night, a Stay tax and a Meal day-pack.
2019-08-20 18:26:42	@OdyX	So yes, it's a three-meal plan twice.
2019-08-20 18:26:52	@OdyX	(Friday Dinner to Sunday Lunch)
2019-08-20 18:27:52	@OdyX	#action OdyX to clarify the extent of the debian.ch support towards the event with the debian.ch board.
2019-08-20 18:28:01	@OdyX	Let's move to Sponsors…
2019-08-20 18:28:09	ℹ 	OdyX has changed topic for #debian.ch from "miniDebConf Vaumarcus IRC meeting | #topic Finances" to "miniDebConf Vaumarcus IRC meeting | #topic Sponsors"
2019-08-20 18:28:36	@OdyX	There I'd really love if someone else could take this topic on.
2019-08-20 18:29:12	@OdyX	Send nice emails, collect promises; setup compensations, etc.
2019-08-20 18:29:51	@OdyX	It needs to be done "soon" too.
2019-08-20 18:30:01	@OdyX	I've added some names on the minutes too.
2019-08-20 18:30:26	@OdyX	Just a short discussion:What we can we offer?
2019-08-20 18:30:44	@OdyX	Sponsor posters, a talk, 1-3 free attendances?
2019-08-20 18:30:56	XTaran	OdyX: I will think of a few more potential sponsors, too, but I won't engage further in that direction.
2019-08-20 18:31:08	XTaran	OdyX: Logos on something?
2019-08-20 18:31:12	jathan	We can offer talks to the sponsors and the possibility of some mini Job fair.
2019-08-20 18:31:40	@OdyX	A2/A1 posters max, perhaps a banner?
2019-08-20 18:32:02	jathan	I think we could reuse the same Art Design and Logos of DebConf13. They were awesome.
2019-08-20 18:32:21	@OdyX	I didn't do much more for the refreshed logo.
2019-08-20 18:32:29	@OdyX	I think I still have the flag :-P
2019-08-20 18:32:30	XTaran	OdyX: I like it! :-)
2019-08-20 18:32:38	@OdyX	Cool.
2019-08-20 18:33:05	@OdyX	#action OdyX to try convincing hug to take on Sponsoring, or find someone else. Last resort, do it.
2019-08-20 18:33:40	jathan	Is this the refreshed logo right? https://wiki.debian.org/DebianEvents/ch/2019/Vaumarcus
2019-08-20 18:33:42	jathan	:)
2019-08-20 18:33:56	XTaran	jathan: I think so.
2019-08-20 18:34:17	@OdyX	Yep. The rendering is weird, but https://salsa.debian.org/debian-ch-team/minidebconf19/tree/master/artwork is the canonical location for these.
2019-08-20 18:34:47	ℹ 	OdyX has changed topic for #debian.ch from "miniDebConf Vaumarcus IRC meeting | #topic Sponsors" to "miniDebConf Vaumarcus IRC meeting | #topic Schedule"
2019-08-20 18:34:47	jathan	Are you thinking to give again some nice red bags to the attendees?
2019-08-20 18:35:11	@OdyX	That's quite low on my list I must say.
2019-08-20 18:35:11	XTaran	OdyX: You mean the talks schedule?
2019-08-20 18:35:29	@OdyX	The thing that's low is the badges :-)
2019-08-20 18:35:59	@OdyX	XTaran: both actually: deciding on a day schedule that makes sense; and filling it with talks.
2019-08-20 18:36:13	XTaran	OdyX: Talks is actually where I intend to join the orga.
2019-08-20 18:36:20	@OdyX	Ahhhhh :-)))))
2019-08-20 18:36:30	@OdyX	EXCELLENT :evil_grin:
2019-08-20 18:37:02	XTaran	Already was thinking about the needed infrastructure for talk submissions and selections.
2019-08-20 18:37:33	@OdyX	XTaran: for what I'm concerned, the field is all yours. As "main orga" (whatever that means), what I kinda need is to know when we want to have the meals, but for the rest, it's open. :-)
2019-08-20 18:37:49	XTaran	Ranges from Debian Wiki to pretix/pretalx/frab.
2019-08-20 18:37:50	jathan	XTaran: Yes, talks submissions should start in the next weeks.
2019-08-20 18:38:04	@OdyX	XTaran: afaik Hamburg did wiki: https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg#talk_schedule_v1.0.1
2019-08-20 18:38:17	XTaran	OdyX: Thanks, will have a look at their workflow.
2019-08-20 18:38:30	@OdyX	and I kinda like the idea to have everything there.
2019-08-20 18:38:56	XTaran	OdyX: /me too, I just can't currently imagine the right workflow for it.
2019-08-20 18:39:04	@OdyX	XTaran: could you #action yourself to what you see yourself doing ? 
2019-08-20 18:39:40	@OdyX	It'd be probably good to decide on the global schedule (day start, meals) on the wiki.
2019-08-20 18:39:57	XTaran	#action XTaran will come up with probably wiki-based workflow for collecting and sorting talk submissions. Role model could be https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg#talk_schedule_v1.0.1
2019-08-20 18:40:24	@OdyX	Nice. Anythings worth discussing on that topic?
2019-08-20 18:40:38	XTaran	OdyX: Just a moment.
2019-08-20 18:41:10	XTaran	OdyX: Yes, regarding the global schedule: From when to when should be talks?
2019-08-20 18:41:51	@OdyX	I kinda like Hamburg's 10-19
2019-08-20 18:41:56	jathan	From Friday morning to Sunday evening.
2019-08-20 18:42:01	XTaran	OdyX: On three or two days?
2019-08-20 18:42:09	XTaran	OdyX: Ah, so three days.
2019-08-20 18:42:13	jathan	Three
2019-08-20 18:42:14	@OdyX	Let me check the contract
2019-08-20 18:42:25	@OdyX	I'm not sure we have the venue that early on Friday
2019-08-20 18:43:08	@OdyX	I booked from 14h30
2019-08-20 18:43:34	@OdyX	#action OdyX to get the venue from Friday morning, without first Lunch (attendees' picnic)
2019-08-20 18:43:38	@OdyX	^right ?
2019-08-20 18:43:59	jathan	I see.
2019-08-20 18:44:31	XTaran	OdyX: Hmmm. So Friday isn't just arrival + social gathering?
2019-08-20 18:44:44	jathan	At what time we need to pick the stuff and clean on Sunday?
2019-08-20 18:44:56	XTaran	OdyX: Means that some might want to arrive already on Thursday...
2019-08-20 18:44:57	@OdyX	I'd say Friday 15-19, Saturday 10-13+15-19, Sunday 10-13 and closing ceremony at 15.
2019-08-20 18:45:06	axhn	Appearently not - although I'll need at least four hours to get there, so 1430 is not that bad for me :)
2019-08-20 18:45:07	XTaran	Ok.
2019-08-20 18:45:11	@OdyX	XTaran: that was the initial idea yes.
2019-08-20 18:45:24	XTaran	OdyX: Thanks for the specific times.
2019-08-20 18:45:31	@OdyX	There's a difference between when we have the venue, and when we have attendees.
2019-08-20 18:45:49	XTaran	OdyX: *nod*
2019-08-20 18:45:51	@OdyX	#info for now, the contract covers Friday 14:30 until Sunday 18:00.
2019-08-20 18:46:20	jathan	OdyX: Thanks for the times details.
2019-08-20 18:47:03	jathan	It is possible to extend the contract since Thursday at 14:30 or later?
2019-08-20 18:47:07	@OdyX	I can certainly arrange for earlier access for specific people (orga team, video team) and organize a picnic, but we shouldn't get attendees much earlier than Friday 14:30 I'd say.
2019-08-20 18:47:24	XTaran	Agree.
2019-08-20 18:48:02	@OdyX	jathan: sure, I'll ask if we can access the building for setup since Thursday, ideally for free.
2019-08-20 18:48:08	 	alphanet (~ircuser@46.140.72.222) has joined #debian.ch
2019-08-20 18:48:24	alphanet	(late hello)
2019-08-20 18:48:30	@OdyX	#info OdyX to clarify buildings' access before Friday (Thursday 14:30)
2019-08-20 18:48:44	@OdyX	alphanet: https://mensuel.framapad.org/p/minidebconfvaumarcus-20190820 is the agenda+minutes.
2019-08-20 18:49:39	@OdyX	XTaran: is it a workable first base ?
2019-08-20 18:49:47	XTaran	OdyX: Definitely.
2019-08-20 18:49:50	jathan	OdyX: Great :) But for attendees will be then on Friday since 14:30 hrs? Could we have the whole Friday to schedule talks since morning or that would be hard to arrange with Le Camp?
2019-08-20 18:50:01	XTaran	OdyX: Will need to take a day off, but that should be doable.
2019-08-20 18:50:26	@OdyX	jathan: it's a remote place, not in a city, so our initial expectation was to do a "small" weekend.
2019-08-20 18:50:39	XTaran	jathan: I think, starting on Friday morning is a bad idea.
2019-08-20 18:50:56	XTaran	jathan: People need to get there first.
2019-08-20 18:51:16	jathan	I understand. Thinking about the weekend idea it is make sense as you say :)
2019-08-20 18:51:32	@OdyX	Let's stick to mid-Friday-until Sunday-but-no-dinner-there.
2019-08-20 18:51:57	@OdyX	Once the venue is cleaned up and emptied, we can certainly have a nice dinner in Yverdon-les-Bains.
2019-08-20 18:52:17	@OdyX	It can be organized when we're actually there, so let's move on. :-)
2019-08-20 18:52:25	XTaran	Agree.
2019-08-20 18:52:48	ℹ 	OdyX has changed topic for #debian.ch from "miniDebConf Vaumarcus IRC meeting | #topic Schedule" to "miniDebConf Vaumarcus IRC meeting | #topic Video Team"
2019-08-20 18:53:01	alphanet	OdyX: thanks
2019-08-20 18:53:03	@OdyX	I guess that's jathan's  :-)
2019-08-20 18:54:12	@OdyX	jathan: as far as I'm concerned, my hope was to have video "magically".
2019-08-20 18:54:30	@OdyX	But I'm aware that you might need things in place, and time for setup, or hardware.
2019-08-20 18:55:33	jathan	OdyX: Perfect! :D
2019-08-20 18:55:47	@OdyX	jathan: how are things looking from the Video Team point of view?
2019-08-20 18:57:08	jathan	OdyX: I have not talked with the team, since I was waiting a response of anyone of Europe.
2019-08-20 18:57:31	@OdyX	jathan: would you be interested in a collaboration with a swiss team, or is the DebConf Video Team autonomous ?
2019-08-20 18:57:33	jathan	Currently I do not know.
2019-08-20 18:58:18	jathan	My last answer was regarding to the Video Team point of view
2019-08-20 18:58:18	@OdyX	jathan: could it work with you alone?
2019-08-20 18:59:26	jathan	OdyX: Alone without other Video Team members?
2019-08-20 19:00:03	jathan	That would be the Swiss Team collaboration?
2019-08-20 19:01:04	@OdyX	jathan: apparently the CCCZH has a VOC (Video Operating Center) who could help.
2019-08-20 19:01:16	XTaran	jathan: There is a chvoc team in the CCC Zurich context.
2019-08-20 19:02:22	XTaran	jathan: They IIRC have equipement for recording and streaming IIRC two tracks, but not much man power.
2019-08-20 19:02:31	@OdyX	jathan: it's just that I expected some more people from the Video Team.
2019-08-20 19:02:36	XTaran	jathan: Can give you contacts if interested.
2019-08-20 19:02:55	@OdyX	perhaps our late event revival hasn't helped.
2019-08-20 19:03:30	@OdyX	I just don't want you to burnout doing this by yourself alone.
2019-08-20 19:03:53	jathan	I see. Yes, I could commit myself to help with the camera's operation during the talks :)
2019-08-20 19:04:46	@OdyX	What we need now is someone "making video coverage happen" (or saying "it's not realistic, let's not try).
2019-08-20 19:04:47	XTaran	jathan: JFTR: Haven't talked to the chvoc yet.
2019-08-20 19:04:56	jathan	Not alone, but at least with some technical pals at the audio and direction.
2019-08-20 19:05:13	@OdyX	jathan: I'm afraid we have a confusion :-)
2019-08-20 19:05:17	XTaran	jathan: But I know their goal is to cover FLOSS events in Switzerland and close-by. So it should fit their focus.
2019-08-20 19:05:43	jathan	I will send an Email to Video Conf Mailing list asking who will attend MiniDebConf in Vaumarcus and see who is available to dome more.
2019-08-20 19:05:49	XTaran	jathan: So the question is: Do you want/need their help?
2019-08-20 19:06:11	@OdyX	jathan: I have not coordinated anything else than writing to the Video Team list; there's no hardware prepared by us at all.
2019-08-20 19:06:24	@OdyX	jathan: ah, yes, good.
2019-08-20 19:06:25	jathan	OdyX: Pardon, yes I got lost. Some confusion.
2019-08-20 19:06:52	@OdyX	No problem. I'm aware we're late to the game.
2019-08-20 19:07:29	jathan	So. From Debian Video Team there are not hardware available to take to Vaumarcus?
2019-08-20 19:07:42	XTaran	jathan: We don't know.
2019-08-20 19:08:15	jathan	XTaran: Ok, I am understanding better :)
2019-08-20 19:08:25	@OdyX	Given the information we have had until now (mostly your messages), not that we're aware, no.
2019-08-20 19:08:46	@OdyX	#action jathan to check with the Video Team to see if something will happen from the DebConf Video Team.
2019-08-20 19:08:55	@OdyX	jathan: would that work for you ^ ?
2019-08-20 19:09:46	jathan	And by the other hand we have the possibility to get help from the Zurich CHVOV Team right?
2019-08-20 19:10:05	XTaran	jathan: I assume. Shall I ask them?
2019-08-20 19:10:07	jathan	OdyX: Ok, yes I will send an Email today to Video Team.
2019-08-20 19:10:46	@OdyX	Cool.
2019-08-20 19:11:25	jathan	XTaran: I could do it. We could wait until Friday to have an answer and if not, let procedure to the alternative with the CHVOV Team of Zurich and me taking care of the recordings.
2019-08-20 19:12:14	ℹ 	OdyX has changed topic for #debian.ch from "miniDebConf Vaumarcus IRC meeting | #topic Video Team" to "miniDebConf Vaumarcus IRC meeting | #topic Organization"
2019-08-20 19:12:15	jathan	I think there will be more friends of Debian Video Team willing to help.
2019-08-20 19:12:35	XTaran	jathan: Ok.
2019-08-20 19:12:57	@OdyX	jathan: the role we need to fill is someone making the video hardware show up, set it up, and take it back.
2019-08-20 19:13:16	XTaran	jathan: I just got a confirmation that at least hardware for one track would be available that weekend.
2019-08-20 19:13:32	@OdyX	Cool !
2019-08-20 19:13:39	XTaran	jathan: Second track/camera set not yet known.
2019-08-20 19:14:17	XTaran	Should be able to figure out during the CCCamp.
2019-08-20 19:14:53	@OdyX	As for the organization; the weekend is at the end of W43, we're W34, that's 9 weeks.
2019-08-20 19:15:18	@OdyX	So let's try to IRC-meet twice in between? Every 3 weeks?
2019-08-20 19:15:24	@OdyX	or less/more ?
2019-08-20 19:15:41	jathan	XTaran: Thanks.
2019-08-20 19:16:56	XTaran	jathan: Can you post on debian-switzerland@l.d.o when you know if the Debian Video Team can provide coverage or not, so that I can then nail down things with the CHVOC?
2019-08-20 19:17:03	jathan	So at least we could have some video hardware from the CCCamp friends for one track and we just need some responsible taking care of it (like me) right?
2019-08-20 19:17:40	jathan	Ok. Could I send the Email asking to Video Team to both Mailing Lists?
2019-08-20 19:17:45	@OdyX	Sure.
2019-08-20 19:17:57	XTaran	jathan: We can have one camera set including one person for sure. Still have to find the second guy who maintains the second set.
2019-08-20 19:18:05	jathan	Great. I will do it after meal at work.
2019-08-20 19:18:24	XTaran	jathan: But I'd usually expect two persons per camera set.
2019-08-20 19:19:41	jathan	XTaran: If the first guy helps me to understand the setting and usage of the video equipment, then I could be the second guy for the second set. We could have a little training session on Thursday evening.
2019-08-20 19:19:58	XTaran	jathan: That's what I thought of, yes.
2019-08-20 19:20:08	@OdyX	Sorry to bring us back on the agenda but it's been a long meeting already :-)
2019-08-20 19:20:12	@OdyX	[21:15] <OdyX> So let's try to IRC-meet twice in between? Every 3 weeks?
2019-08-20 19:20:25	jathan	But let's see what is going on with Debian Video Team this week :)
2019-08-20 19:20:33	XTaran	jathan: Ok. Thanks!
2019-08-20 19:20:53	jathan	OdyX: Yes, I could attend our IRC meetings at this time like today.
2019-08-20 19:21:17	@OdyX	XTaran: thoughts about 2 more meetings? Enough? Too much?
2019-08-20 19:21:37	jathan	Even shorter each two week since we are on the middle of August.
2019-08-20 19:21:40	XTaran	OdyX: Actually no gut feeling.
2019-08-20 19:22:30	XTaran	OdyX: Only two IRC meetings seem not enoughh.
2019-08-20 19:22:35	jathan	September 3rd the next one? What do you say?
2019-08-20 19:22:45	XTaran	... too little.
2019-08-20 19:22:48	jathan	Or earlier
2019-08-20 19:23:15	jathan	Next week to have an update status of what we have talked and organised today.
2019-08-20 19:23:19	XTaran	jathan: That's the next Debian Meetup in Zurich. So real-life meeting?
2019-08-20 19:23:31	XTaran	3. September 2019: Debian-Treff Zürich
2019-08-20 19:23:39	jathan	XTaran: I would love to do it, but I am in Mexico City :)
2019-08-20 19:23:47	XTaran	jathan: Oops. :)
2019-08-20 19:24:01	XTaran	jathan: Then count this date conflict. :-)
2019-08-20 19:24:05	XTaran	+as
2019-08-20 19:24:12	jathan	So remotely for me is possible when you want guys.
2019-08-20 19:24:19	@OdyX	Let's do every two weeks then, I agree.
2019-08-20 19:24:36	jathan	I agree too.
2019-08-20 19:24:40	@OdyX	Tuesdays are no good for me, and Zurich is far.
2019-08-20 19:25:03	@OdyX	Wednesday 4. IRC, ?
2019-08-20 19:25:08	jathan	I could at any day from Monday to Friday.
2019-08-20 19:25:09	@OdyX	20h CEST?
2019-08-20 19:25:27	jathan	OdyX: Great. I agree :=
2019-08-20 19:25:29	XTaran	OdyX: Ok for me.
2019-08-20 19:25:30	jathan	:?
2019-08-20 19:25:34	jathan	:)
2019-08-20 19:25:51	@OdyX	#info Next meeting Wednesday 4. September, 20h CEST.
2019-08-20 19:26:01	jathan	Got it.
2019-08-20 19:26:12	ℹ 	OdyX has changed topic for #debian.ch from "miniDebConf Vaumarcus IRC meeting | #topic Organization" to "miniDebConf Vaumarcus IRC meeting | #topic Bar"
2019-08-20 19:26:16	@OdyX	Two more topics.
2019-08-20 19:26:26	@OdyX	Any idea on how to coordinate a Bar?
2019-08-20 19:26:45	@OdyX	#action OdyX to check with Le Camp what they offer / what are their conditions.
2019-08-20 19:27:18	@OdyX	Worst case, I'll care for a large amount of beer and sell it cost-equivalent.
2019-08-20 19:27:38	jathan	Yeah, that is practical.
2019-08-20 19:27:47	@OdyX	#action OdyX to check for a tap-beer offer.
2019-08-20 19:28:01	@OdyX	I'll actually email a beer company I know to see what their prices could be.
2019-08-20 19:28:24	@OdyX	I don't see us needing much more than a double-tap pressure beer.
2019-08-20 19:28:27	jathan	Because other option could be to rent a Bar service on place, but sure will be more expensive.
2019-08-20 19:28:39	@OdyX	Again, if another local could handle this :8)
2019-08-20 19:28:46	@OdyX	alphanet maybe ?
2019-08-20 19:29:09	alphanet	if I remember well from debconf, there was some kind of "droit de bouchon", aka you can't sell your stuff but must go through them?
2019-08-20 19:29:23	@OdyX	I can check with Le Camp anyway, but caring for a tapbeer/bar offering could be interesting anyway.
2019-08-20 19:30:01	alphanet	also alcoholic beverages might need a license
2019-08-20 19:30:19	@OdyX	alphanet: my point is: besides checking with Le Camp, could you take on organizing a bar offering (maybe after I've checked with Le Camp) ?
2019-08-20 19:30:52	alphanet	(the only time I arranged for some bar was in 2009 for a film festival, and we did it ourselves with a staff of 3+ people, no alcoholic beverages)
2019-08-20 19:30:57	XTaran	OdyX: I might be able to man the bar for a shift or two, but since I don't drink alcohol, I feel like being the completely wrong person for organizing beer. ;-)
2019-08-20 19:31:25	@OdyX	Okay. I'll see what I can do.
2019-08-20 19:31:28	alphanet	OdyX: I can investigate it, I doubt I can organize it fully.
2019-08-20 19:31:58	@OdyX	Let's all try to find someone.
2019-08-20 19:32:08	alphanet	OdyX: first, ask Le Camp what is doable, and when you know, I will investigate further
2019-08-20 19:32:09	ℹ 	OdyX has changed topic for #debian.ch from "miniDebConf Vaumarcus IRC meeting | #topic Bar" to "miniDebConf Vaumarcus IRC meeting | #topic Varia"
2019-08-20 19:32:35	@OdyX	alphanet: excellent, thanks.
2019-08-20 19:32:55	jathan	What is Varia please?
2019-08-20 19:33:06	alphanet	jathan: "Divers"?
2019-08-20 19:33:38	XTaran	jathan: miscellany
2019-08-20 19:33:49	@OdyX	jathan: any topic important enough and not yet covered.
2019-08-20 19:34:08	XTaran	jathan: s/miscellany/miscellaneous/
2019-08-20 19:34:10	jathan	Thank you for the explanation :)
2019-08-20 19:34:25	alphanet	where are you all from if I may ask (me: 43 km from Le Camp)
2019-08-20 19:34:29	@OdyX	For me it seems we have most things covered.
2019-08-20 19:34:35	@OdyX	alphanet: Vevey.
2019-08-20 19:34:37	XTaran	alphanet: Zurich
2019-08-20 19:35:09	alphanet	so, I guess, I am THE local
2019-08-20 19:35:11	alphanet	:->
2019-08-20 19:35:48	jathan	Maybe just to say at the MiniDebConf Vaumarcus some info about the weather during those days of October and what to bring like in DC13 (towells, soap, blankets)
2019-08-20 19:36:06	@OdyX	AFAIK you've always been. Raphael was living in Neuchatel in 13.
2019-08-20 19:36:16	@OdyX	jathan: ah yes, good point.
2019-08-20 19:36:52	jathan	MiniDebConf Vaumarcus Website I missed.
2019-08-20 19:37:03	XTaran	jathan: https://wiki.debian.org/DebianEvents/ch/2019/Vaumarcus
2019-08-20 19:37:31	alphanet	local weather can be warm or cold; Le Camp is not very high (~500m) so the temperature should be similar to the Plateau
2019-08-20 19:37:53	alphanet	OdyX: Neuchâtel is a bit closer than Fontainemelon to Le Camp
2019-08-20 19:37:53	@OdyX	I'll try to write down meaningful minutes and send them to the list really soon (tomorrow)k
2019-08-20 19:38:08	jathan	It coul be windy and colder with humidity because the lake?
2019-08-20 19:38:37	alphanet	jathan: humidity yes (although that would be more November generally), wind yes, but probably not that bad
2019-08-20 19:38:52	jathan	alphanet: Thanks.
2019-08-20 19:38:54	alphanet	jathan: anyway it's too early for weather forecasting :)
2019-08-20 19:39:26	alphanet	also, in case someone needs hosting before the conference, this could be arranged at my place
2019-08-20 19:39:39	@OdyX	late October in Switzerland can be chilly. Expect anything between 5 to 17 degrees C.
2019-08-20 19:39:45	alphanet	although it's not that close (~1h30 by public transportation, 40min by car)
2019-08-20 19:39:57	jathan	Great :)
2019-08-20 19:40:08	@OdyX	anyway. We're clearly in the post-meeting chat, so let's free the CCCamp gatherers.
2019-08-20 19:40:11	@OdyX	#endmeeting
```
